-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Jan 2020 pada 02.20
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kfclub`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fu`
--

CREATE TABLE `tb_fu` (
  `id_kfc` varchar(7) NOT NULL,
  `tgl_masuk_fu` date NOT NULL,
  `fu` varchar(50) NOT NULL,
  `i` text NOT NULL,
  `ii` text NOT NULL,
  `iii` text NOT NULL,
  `goal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_fu`
--

INSERT INTO `tb_fu` (`id_kfc`, `tgl_masuk_fu`, `fu`, `i`, `ii`, `iii`, `goal`) VALUES
('KFC1617', '2019-12-29', 'JOY ADIA PRAJOGO', 'baru wa', 'ketemuan', 'ga tau kemana orangbya', 'komsel'),
('KFC1618', '2019-12-29', 'FEBBY BUDI SANTOSA', 'sudah cal', 'ikut komsel', '', 'komsel joyfull'),
('KFC1619', '2019-10-29', 'DEDY KUSWORO', 'kontak wa', 'dateng youth', 'fgfghdsjfdgjsfgjnfgjfgjfgjfgfgjfgjdfjdfhfhgstrhgshfdsjhsedfhfdghxgddhgdfgfdgtgsdg', ''),
('KFC1620', '2019-09-29', 'BABE YOHAN', 'kontak wa', 'dateng youth', 'spy', 'komsel unique'),
('KFC1621', '2019-12-29', 'NITA KUSUMAWATI', 'ketemuan', 'ikut spy', '', 'komsel Banyak'),
('KFC1622', '2019-12-30', 'DEDI SUPRIYANTO', 'kontak wa', 'ikut spy', 'fhxgfgjjcd', ''),
('KFC1623', '2019-12-30', 'DEDY KUSWORO', '', 'afasdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', '', ''),
('KFC1624', '2019-12-30', '', '', '', '', ''),
('KFC1625', '2019-12-30', '', '', '', '', ''),
('KFC1626', '2019-12-30', '', '', '', '', ''),
('KFC1627', '2019-12-30', '', '', '', '', ''),
('KFC1628', '2019-12-30', '', '', '', '', ''),
('KFC1629', '2019-12-30', '', '', '', '', ''),
('KFC1630', '2019-12-30', '', '', '', '', ''),
('KFC1631', '2019-12-30', '', '', '', '', ''),
('KFC1632', '2019-12-30', '', '', '', '', ''),
('KFC1634', '2020-01-10', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_fu`
--
ALTER TABLE `tb_fu`
  ADD PRIMARY KEY (`id_kfc`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
