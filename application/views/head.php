<!--head-->
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content>
<meta name="author" content>
<title>KFClub GBI Gama</title>
<link href="<?php echo base_url('/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo base_url('/bootstrap/css/bootstrap-theme.min.css');?>" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url('/kfc.png');?>">
</head>
<body>
<nav class=" navbar navbar-default  navbar-fix-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

      <a href="<?php echo base_url('index.php/dashboard02/index');?>" class="navbar-brand"><img src="<?php echo base_url('/kfc.png');?>" width="90px" align="top" /></a>
      
    </div>
<!--menu-->
	<div class="collapse navbar-collapse" id="navbar">
		<ul class="nav navbar-nav navbar-right">
			<!--li class="active">
				<a href="<?php echo base_url('index.php/dashboard02/index');?>">Dashboard</a>
			</li>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Youth Party<span class="caret"></a>
				<ul class="dropdown-menu">
				<li><a href="<?php echo base_url('index.php/dashboard/jemaat_baru');?>"><span class="glyphicon glyphicon-plus"></span>  Tambah Jemaat</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/input_presensi');?>"><span class="glyphicon glyphicon-edit"></span>  Input Presensi</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/cetak_presensi');?>"><span class="glyphicon glyphicon-print"></span>  Cetak Presensi</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/info');?>"><span class="glyphicon glyphicon-signal"></span>  Info</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/unactiv');?>"><span class="glyphicon glyphicon-list-alt"></span>  Not Active</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/direct_presensi');?>"><span class="glyphicon glyphicon-list-alt"></span> Presensi</a></li>
				

				</ul>
			</li-->

			<!--li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Info<span class="caret"></a>
				<ul class="dropdown-menu">
				<li><a href="<?php echo base_url('index.php/dashboard/info_ultah');?>"><span class="glyphicon glyphicon-plus"></span> Daftar ultah </a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/info_keluarga_baru');?>"><span class="glyphicon glyphicon-print"></span> Keluarga Baru</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/report_presensi');?>"><span class="glyphicon glyphicon-edit"></span> Report Presensi</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/report_presensi_pelayan');?>"><span class="glyphicon glyphicon-edit"></span> Report Presensi Pelayan</a></li>

				<li><a href="<?php echo base_url('index.php/dashboard/alokasi');?>"><span class="glyphicon glyphicon-edit"></span> Alokasi Pelayan</a></li>
				</ul>
			</li>

			<li><a href="<?php echo base_url('index.php/c_komsel/index');?>">Komsel<span class="caret"></a>
			</li>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Komunitas<span class="caret"></a>
				<ul class="dropdown-menu">
				<li><a href="#">Basket</a></li>
				<li><a href="#">Batminton(Bulu Tangkis)</a></li>
				<li><a href="#">Dance</a></li>
				<li><a href="#">Futsal</a></li>
				<li><a href="#">GMTC</a></li>
				<li><a href="#">GMTC</a></li>
				</ul>
			</li-->
		
			<li >	<a href="<?php echo base_url("/index.php/dashboard/mbuh");?>"><i class="icon-edit"></i><span class="glyphicon "></span>Presensi Kel Baru</a></li>
			<li >	<a href="<?php echo base_url('index.php/dashboard/report_presensi_pelayan');?>"><i class="icon-edit"></i><span class="glyphicon "></span>Report Pelayan</a></li>
			<li >	<a href="<?php echo base_url('index.php/dashboard/alokasi');?>"><i class="icon-edit"></i><span class="glyphicon "></span>Alokasi Pelayan</a></li>
			
		
			<!--li><a href="#"><i>Welcome <?php echo $user['nama_admin'];?></i></a></li>
				<li ><a href="<?php echo base_url('index.php/dashboard02');?>"><i class="icon-edit"></i><span class="glyphicon "></span>YParty V2.0</a></li>
				<li><a href="<?php echo base_url('index.php/dashboard02/direct_presensi_pelayan');?>"><span class="glyphicon glyphicon-list-alt"></span> Presensi Pelayan</a></li-->
			<li ><a href="<?php echo base_url('index.php/dashboard/logout');?>"><i class="icon-edit"></i><span class="glyphicon glyphicon-off"></span></a></li>
		</ul>
	</div>
<!--end menu-->	
  </div>
</nav>
<!--end head-->