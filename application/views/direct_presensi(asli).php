<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Presensi Youth Parthy GBI Gajah Mada</title>


    <!-- Memanggil file .js untuk proses autocomplete -->
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery-1.8.2.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>

    <link href="<?php //echo base_url('/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('/bootstrap/css/bootstrap-theme.min.css');?>" rel="stylesheet">

    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    

    <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/dashboard/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_nama_jemaat').val(''+suggestion.nama_jemaat); // membuat id 'v_nama' untuk ditampilkan
                    
                    $('#v_tlp_jemaat').val(''+suggestion.tlp_jemaat); // membuat id 'v_unitkerja' untuk ditampilkan
                    $('#v_alamat_jemaat').val(''+suggestion.alamat_jemaat);
                    $('#v_komsel').val(''+suggestion.komsel);
                     $('#v_alokasi').val(''+suggestion.alokasi);
                     $('#v_tgl_lahir').val(''+suggestion.tgl_lahir); // membuat id 'v_gol' untuk ditampilkan. nama id alias sedangkan ini alias dr controler
                    $('#v_id_kfc').val(''+suggestion.id_kfc);
                    
                }
            });
        });
    </script>
    <style type="text/scc">
        @font-face{
            font-family: "The Bully_PersonalUseOnly"
        }

        .container1{
            width:400px;
            height:auto;
            padding:20px;
            background:grey;
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -200px;
            margin-left: -220px;
        }
        
    </style>
</head>
<body>


<div class="container">
  <div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
<?php 

  $atribut=array('class'=>'well form-horizontal','id'=>'contact_form');echo form_open('dashboard/input_direct_presensi',$atribut);?>

<h1 align="center" style="font-family: The Bully_PersonalUseOnly; scr: url('TheBully_PersonalUseOnly.ttf');">Presensi Youth Party</h1>
<h4 align="center">GBI Gajah Mada</h4>
<p align="center"><?php $tgl_input=date("Y-m-d"); echo $tgl_input;?></p>

                    <div class="wrap" align="center">

                        <div class="form-group">

                  <label class="col-md-4 control-label"></label>  

                  <div class="col-md-4 inputGroupContainer">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>

                  <input type="search" style="witdh:400px; font-size=150px;" class="autocomplete nama" name="nama_jemaat" id="autocomplete1" placeholder="Ketikan nama disini" autofocus required="required" /> <button type="reset" class="btn btn-danger" value="reset" name="reset"> <span class="glyphicon glyphicon-refresh">   Reset</span></button>

                    </div>

                  </div>

                </div>

                 <div class="form-group">

                  <label class="col-md-4 control-label">Nama</label>  

                    <div class="col-md-4 inputGroupContainer">

                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>

                  <input name="nama_jemaat2" placeholder="Nama" class="autocomplete"  type="text" id="v_nama_jemaat"  readonly/>
                  
                  
                  <input name="tgl_presensi" class="form_control"  type="HIDDEN" value="<?php echo $tgl_input;?>" />

                    </div>

                  </div>

                </div>

                  <input name="id_kfc" placeholder="ID KFC" class="autocomplete"  type="text" id="v_id_kfc" hidden />
                  <input name="alokasi" placeholder="alokasi" class="autocomplete"  type="text" id="v_alokasi" hidden />

                    


                <div class="form-group">

                  <label class="col-md-4 control-label">Telepon</label>  

                    <div class="col-md-4 inputGroupContainer">

                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>

                  <input name="tlp" placeholder="Telepon" class="autocomplete"  type="text" id="v_tlp_jemaat" readonly />

                    </div>

                  </div>

                </div>

                <div class="form-group">

                  <label class="col-md-4 control-label">Alamat</label>  

                    <div class="col-md-4 inputGroupContainer">

                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>

                  <input name="alamat" placeholder="Alamat" class="autocomplete"  type="text" id="v_alamat_jemaat" readonly />

                    </div>

                  </div>

                </div>
                <div class="form-group">

                  <label class="col-md-4 control-label">KOmsel</label>  

                    <div class="col-md-4 inputGroupContainer">

                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-cloud"></i></span>

                  <input name="komsel" placeholder="komsel" class="autocomplete"  type="text" id="v_komsel" readonly/>
                  


                    </div>

                  </div>

                </div>
                <div class="form-group">

                  <label class="col-md-4 control-label">Tanggal Lahir</label>  

                    <div class="col-md-4 inputGroupContainer">

                    <div class="input-group">

                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>

                  <input name="tgl_lahir" placeholder="tgl lahir" class="autocomplete"  type="text" id="v_tgl_lahir" readonly/>


                    </div>

                  </div>

                </div>
                <div class="form-group">

                  <button type="submit" class="btn btn-success" value="submit" name="submit">Hadir <span class="glyphicon glyphicon-send"></span></button>
                  
                </div>

<!--  
    <table>
        <tr>
            <td><small>NIP :</small><br><input type="search" class='autocomplete nama' id="autocomplete1" name="nama_customer"/></td>
        </tr>
        <tr>
            <td><small>Nama :</small><br><input type="text" class='autocomplete' id="v_nama_jemaat" name="nama_customer2"/></td>
        </tr>
        <tr>
            <td><small>Unit Kerja :</small><br><input type="text" class='autocomplete' id="v_tlp_jemaat" name="tlp"/></td>
        </tr>
        <tr>
            <td><small>Golongan :</small><br><input type="text" class='autocomplete' id="v_alamat_jemaat" name="alamat"/></td>
        </tr>
-->
       
    </div>

</div>
<div class="container">
  <table class="table table-condensed table-striped table-responsive">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Gender</th>
                <th>Komsel</th>
				<th>Waktu Hadir</th>
                <th></th>
              </tr>
            </thead>
              <tbody>
              <?php
                $no=1;
                if($data_get==NULL){?>
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <h4>Peringatan</h4>
                <p>Peringatan</p>
              </div>
              <?php }else{foreach($data_get as $dg){?>
                <tr <?php //$warna=$dg->alokasi;
                 //if($warna=="vo"){
                 // echo "class='btn-warning'";}else{if($warna=="mu"){echo "class='btn-success'";}}?>>
                  <td><?php echo $no++;?></td>
                  <td><a href="<?php echo base_url('index.php/dashboard02/profil');?>/<?php echo $dg->id_kfc; ?>"><?php echo $dg->nama_jemaat;?></a></td>
                  <td><?php echo $dg->jenis_kelamin;?></td>
                  <!--td><?php //echo $dg->alamat_jemaat;?></td-->
                  <td><?php echo $dg->komsel;?></td>
				  <td><?php echo $dg->jam_presensi;?></td>
                  <td><a href="<?php echo base_url('index.php/dashboard02/edit_profil/');?>/<?php echo $dg->id_kfc; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                  </td>
        
                </tr>
              <?php } }?>
              <tbody>
          </table>
</div>

<script src="<?php echo base_url('/bootstrap/js/bootstrap.min.js');?>"></script>
</body>
</html>