<!--body-->
<div class="container">

    <form class="well form-horizontal" action="<?php echo base_url('index.php/dashboard/insert_jemaat');?>" method="post"  id="contact_form">
	
<fieldset>

<!-- Form Name -->
<legend>Keluarga Baru...!</legend>



<!-- nama-->

<div class="form-group">
  <label class="col-md-4 control-label">Nama</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="id_kfc" class="form-control" type="HIDDEN" value="<?php echo $id_kfc;?>">
  <input  name="nama" placeholder="<?php echo $id_kfc;?>" class="form-control"  type="text" autofocus required="required">
    </div>
  </div>
</div>

<!--kelamin-->
 <div class="form-group">
                        <label class="col-md-4 control-label">Jenis Kelamin</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="jenis_kelamin" value="L" selected /> Laki-laki
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="jenis_kelamin" value="P" /> Perempuan
                                </label>
                            </div>
                        </div>
                    </div>

<!--Tgl Lahir-->
 
<div class="form-group">
  <label class="col-md-4 control-label">Tanggal Lahir</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="tgl_lahir" placeholder="Tanggal Lahir" class="form-control"  type="date">
    </div>
  </div>
</div>

<!-- Alamat-->
      
<div class="form-group">
  <label class="col-md-4 control-label">Alamat</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="alamat" placeholder="Address" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- telepon-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Telepon</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="tlp" placeholder="(845)555-1212" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Pin BB/ WhatsUp-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Pin BB/ WhatsUp</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="bbm_wa" placeholder="Pin BB/ WhatsUp" class="form-control" type="text">
    </div>
  </div>
</div>

<!--join date-->
 
<div class="form-group">
  <label class="col-md-4 control-label">Join date</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="tgl_masuk" placeholder="Join date" class="form-control"  type="date">
    </div>
  </div>
</div>

<!-- Facebook or Instagram-->
<div class="form-group">
  <label class="col-md-4 control-label">Facebook or Instagram</label>  
   <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
  <input name="fb_ig" placeholder="Facebook or Instagram" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Facebook or Instagram-->
<div class="form-group">
  <label class="col-md-4 control-label">Komsel</label>  
   <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
  <input name="komsel" placeholder="Komsel" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Welcome <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for join us, Selamat bergabung dengan kami di Youth Party GBI GAMA.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-warning" value="submit" name="submit">Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->
<!--end body-->