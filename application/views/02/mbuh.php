<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('/assets/img/apple-icon.png');?>" />
    <link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/favicon.png');?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Kingdom Fun Club GBI Gama</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css');?>" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url('/assets/css/material-dashboard.css?v=1.2.0');?>" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('/assets/css/demo.css');?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="http://kfclub.xyz/kfc.png">
</head>

<body>
    <div class="wrapper">
	<div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
	<div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div> 
<br>

                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Selamat Datang Di KFClub GBI Gajah Mada</h4>
                                    <p class="category">Complete your profile</p>
									
                                </div>
                                
                                <div class="card-content">
                                    <form action="<?php echo base_url('/index.php/dashboard02/input_mbuh');?>" method="post">

                                        
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Nama <?php echo $id_kfc;?></label>
													<input name="id_kfc" class="form-control" type="HIDDEN" value="<?php echo $id_kfc;?>">
                                                    <input type="text" name="nama" class="form-control" autofocus>
                                                </div>
                                            </div>
                                        </div>
									
										
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Jenis Kelamin</label>
                                                    <select name="kelamin" class="form-control">
														<option value="l">Laki-Laki </option>
														<option value="p">Perempuan </option>
													</select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">No. Telepon/HP</label>
                                                    <input type="text" name="tlp" class="form-control">
                                                </div>
                                            </div>
											
                                            </div>
                                             <div class="row">
													 <div class="col-md-6">
														<div class="form-group label-floating">
															<label class="control-label">Tgl Lahir :</label>
														</div>
													</div>
											 </div>
											 
											 <div class="row">
													 <div class="col-md-1">
														<div class="form-group label-floating">
															<label class="control-label">Tanggal</label>
															<input type="text" name="tgl" class="form-control">
														</div>
													</div>
													<div class="col-md-1">
														<div class="form-group label-floating">
															<label class="control-label">Bulan</label>
															<input type="text" name="bln" class="form-control">
														</div>
													</div>
													<div class="col-md-1">
														<div class="form-group label-floating">
															<label class="control-label">Tahun</label>
															<input type="text" name="thn" class="form-control">
														</div>
													</div>

											 </div>
											                                  <!---->
										<div class="row">
                                            <div class="col-md-4">
     <div class="form-group label-floating">
        <label class="control-label">Instagram</label>
            <input type="text" name="fb_ig" class="form-control">
     
				                        </div>
                                            </div>
                                            
                                            <div class="col-md-8">
    <div class="form-group label-floating">
        <label class="control-label">Whatsapp</label>
            <input type="text" name="bbm_wa" class="form-control">
                                                </div>
                                            </div>
											
                                            </div>
                                                                          <!---->
											 
                                        <button type="submit" value="submit" name="submit" class="btn btn-primary pull-left"><i class="material-icons">done_all</i> Update</button>
                                        <div class="clearfix"></div>
										<a class="pull-right" title="Back to Home" href="<?php echo base_url("/index.php/dashboard02/direct_presensi");?>" >
                                                        <i class="material-icons">home</i> Ke Presensi Utama
                                                        <div class="ripple-container"></div>
                                                    </a>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
			</div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo base_url('/assets/js/jquery-3.2.1.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/js/material.min.js');?>" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url('/assets/js/chartist.min.js');?>"></script>
<!--  Dynamic Elements plugin -->
<script src="<?php echo base_url('/assets/js/arrive.min.js');?>"></script>
<!--  PerfectScrollbar Library -->
<script src="<?php echo base_url('/assets/js/perfect-scrollbar.jquery.min.js');?>"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url('/assets/js/bootstrap-notify.js');?>"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url('/assets/js/material-dashboard.js?v=1.2.0');?>"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('/assets/js/demo.js');?>"></script>
<script>   
    $('#notifications').slideDown('slow').delay(3000).slideUp('slow');
</script>

</html>