<!--autocomplete-->
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery-1.8.2.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />
    <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/dashboard/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_nama_jemaat').val(''+suggestion.nama_jemaat); // membuat id 'v_nama' untuk ditampilkan
                    
                    $('#v_id_kfc').val(''+suggestion.id_kfc);
                }
            });
        });
    </script>
    <!--end autocomplete-->
<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Profil </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
					<div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
                    <div class="row">
                        
						<div class="col-md-4">
                            <!--mulai edit-->
                            <div class="card card-profile">
                               

                                <div class="card-avatar " data-background-color="blue">
                                    <!--h4 class="title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?>Data Presensi</h4-->
                                    <img class="img" src="<?php 
                                    echo base_url();?>foto/<?php foreach($data_profil as $data){ 
                                        
                                        if($data->foto==null){
                                            echo "user.jpg";
                                        }else{echo $data->foto;}
                                    }
                                        ?>" width="100px">
                                    <div class="row">
                                        <!--div class="col-md-4">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->nama_jemaat;}?></p><br>
                                            
                                            </div>
                                        <div class="col-md-7">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->id_kfc;}?></p>
                                                    
                                        </div-->

                                    </div>
									

                                </div>

                                <div class="card-content card-body ">
                                    <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                    <h4 class="card-title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?></h4>
                                    <h6 class="card-category text-danger <?php foreach($data_profil as $data){ 
                                        if($data->nama_jemaat=="0"){echo "text-danger";}else{echo "text-info";}}?>"><?php foreach($data_profil as $data){ 
                                        if($data->nama_jemaat=="0"){echo "Not Active";}else{echo "Active";}}?></h6>
                                    <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="<?php foreach($data_profil as $data){ echo base_url('index.php/dashboard02/edit_profil/'); echo $data->id_kfc;} ?>" class="btn btn-just-icon btn-danger btn-round">Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="green">
                                    <h4><?php foreach($data_profil as $data){ echo $data->id_kfc;}?></h4>
                                    <!--img src="<?php echo base_url();?>foto/komsel/<?php foreach($data_profil as $data){ 
                                        if($data->komsel=='unique'){
                                            echo 'unique.png';
                                    }else{if($data->komsel=='banyak'){
                                            echo 'banyak.png';
                                    }else{if($data->komsel=='jco'){
                                            echo 'jco.png';
                                    }else{if($data->komsel=='rock'){
                                            echo 'rock.png';
                                    }else{if($data->komsel=='gamalove'){
                                            echo 'gamalove.png';
                                    }else{echo 'pemenang.png';}}}
                                }
                                    }
                                        }?>" -->
                                    
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active">
                                            <table class="table">
 <?php $atribut=array();echo form_open('dashboard02/simpan_fu',$atribut);?>
                                                <tr>
                                                    <td>Petugas</td><td>
 <input type="search" style="witdh:400px; font-size=150px;" class="autocomplete nama" name="nama_jemaat" id="autocomplete1" placeholder="Ketikan nama disini" autofocus  size="35" /> <button type="reset" class="btn btn-danger" value="reset" name="reset"> <span class="glyphicon glyphicon-refresh">   Reset</span></button><br>
                                            </tr>
                                                <tr>
                                                    <td>iD Petugas</td><td>
                                                        <input name="fu" placeholder="ID KFC" class="autocomplete"  type="text" value="<?php foreach($data_profil as $data){ echo $data->fu;}?>" id="v_nama_jemaat" readonly="" />
                                                        <input name="id_kfc" placeholder="ID KFC" class="autocomplete"  type="text" value="<?php foreach($data_profil as $data){ echo $data->id_kfc;}?>" hidden />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tahap 1</td><td><textarea width="50px" height="20px" name="i" value="<?php foreach($data_profil as $data){ echo $data->i;}?>" id="" <?php foreach($data_profil as $data){ if($data->i!=null){echo "readonly=''";}}?>></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Tahap 2</td><td><textarea name="ii" value="<?php foreach($data_profil as $data){ echo $data->ii;}?>" id="" <?php foreach($data_profil as $data){ if($data->ii!=null){echo "readonly=''";}}?>></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Tahap 3</td><td><textarea  name="iii" value="<?php foreach($data_profil as $data){ echo $data->iii;}?>" id="" <?php foreach($data_profil as $data){ if($data->iii!=null){echo "readonly=''";}}?>></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Goal</td><td><textarea name="goal" value="<?php foreach($data_profil as $data){ echo $data->goal;}?>" id="" <?php foreach($data_profil as $data){ if($data->goal!=null){echo "readonly=''";}}?>></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Follow Up</td><td>None<br>
                                                    <button type="submit" class="btn btn-success" value="submit" name="submit" width="100px">Simpan <span class="glyphicon glyphicon-send"></span></button></td>
                                                </tr>
                                            </table>
                                            
                                            <div class="card-footer justify-content-center card-profile">
                                                    <a href="https://api.whatsapp.com/send?phone=62<?php foreach($data_profil as $data){ echo ltrim(($data->tlp_jemaat),'0');}?>&text=Happy Birthday" class="btn btn-just-icon btn-whatsapp btn-round" data-background-color="green">
                                                        <i class="fa fa-whatsapp"></i>
                                                    </a>
                                                    
                                                    <a href="tel:<?php foreach($data_profil as $data){ echo $data->tlp_jemaat;}?>" class="btn btn-just-icon btn-google btn-round">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
<script src="<?php echo base_url('/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('/assets/js/material.min.js');?>" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url('/assets/js/chartist.min.js');?>"></script>
<!--  Dynamic Elements plugin -->
<script src="<?php echo base_url('/assets/js/arrive.min.js');?>"></script>
<!--  PerfectScrollbar Library -->
<script src="<?php echo base_url('/assets/js/perfect-scrollbar.jquery.min.js');?>"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url('/assets/js/bootstrap-notify.js');?>"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url('/assets/js/material-dashboard.js?v=1.2.0');?>"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

<script type="text/javascript">
    $(document).ready(function() {

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>