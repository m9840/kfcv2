<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('/assets/img/apple-icon.png');?>" />
    <link rel="icon" type="image/png" href="<?php echo base_url('/kfc.png');?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>KFClub GBI Gama</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css');?>" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url('/assets/css/material-dashboard.css?v=1.2.0');?>" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('/assets/css/demo.css');?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
	 <script src="<?php echo base_url('/chart/Chart.bundle.js');?>"></script>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="<?php echo base_url('/assets/img/sidebar-1.jpg');?>">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="<?php echo base_url();?>" class="simple-text">
                    Kngdom Fun Club
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="<?php echo base_url("/index.php/c_fkb/admin");?>">
                            <i class="material-icons">dashboard</i>
                            <p>Home</p>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo base_url("/index.php/c_fkb/pembina");?>">
                            <i class="material-icons">assignment_ind</i>
                            <p>Pembina</p>
                        </a>
                    </li>
                    <li >
                        <a href="<?php echo base_url("/index.php/dashboard/mbuh");?>">
                            <i class="material-icons">face</i>
                            <p>Presensi Kel Baru</p>
                        </a>
                    </li>
					
                    <li>
                        <a href="<?php echo base_url("/index.php/dashboard02/jemaat");?>">
                            <i class="material-icons">person</i>
                            <p>Data Anggota</p>
                        </a>
                    </li>
					<li>
                        <a href="<?php echo base_url("/index.php/dashboard02/presensi");?>">
                            <i class="material-icons">bubble_chart</i>
                            <p>Presensi</p>
                        </a>
                    </li>
					<li>
                        <a href="<?php echo base_url("/index.php/dashboard02/info_keluarga_baru");?>">
                            <i class="material-icons">people</i>
                            <p>Jemaat baru</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url("/index.php/c_fkb/admin");?>">
                            <i class="material-icons">people</i>
                            <p>FKB</p>
                        </a>
                    </li>
                    <!--li>
                        <a href="<?php //echo base_url("/index.php/dashboard02/absen");?>">
                            <i class="material-icons">content_paste</i>
                            <p>Visitasi List </p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php //echo base_url("/index.php/dashboard02/ranking");?>">
                            <i class="material-icons">format_list_numbered</i>
                            <p>Rangking</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li-->
					<li>
                        <a href="<?php echo base_url("/index.php/dashboard02/logout");?>">
                            <i class="material-icons">power_settings_new</i>
                            <p>Log Out</p>
                        </a>
                    </li>
                    
                    <li class="active-pro">
                        <a href="#">
                            <i class="material-icons">unarchive</i>
                            <p>Upgrade to PRO</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>