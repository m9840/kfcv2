 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery-1.8.2.min.js'></script>
    <link href="<?php echo base_url('/bootstrap/css/bootstrap-theme.min.css');?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300i,400,700" rel="stylesheet">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>tema_baru/css/style.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>tema_baru/fonts/icomoon/style.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>tema_baru/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>tema_baru/css/owl.carousel.min.css">
 	<!--autocomplete-->
 	<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>
 	<link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />
 	<script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/dashboard/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_nama_jemaat').val(''+suggestion.nama_jemaat); // membuat id 'v_nama' untuk ditampilkan
                    
                    $('#v_tlp_jemaat').val(''+suggestion.tlp_jemaat); // membuat id 'v_unitkerja' untuk ditampilkan
                    $('#v_alamat_jemaat').val(''+suggestion.alamat_jemaat);
                    $('#v_komsel').val(''+suggestion.komsel);
                     $('#v_alokasi').val(''+suggestion.alokasi);
                     $('#v_tgl_lahir').val(''+suggestion.tgl_lahir); // membuat id 'v_gol' untuk ditampilkan. nama id alias sedangkan ini alias dr controler
                     $('#v_fb_ig').val(''+suggestion.fb_ig);
                    $('#v_id_kfc').val(''+suggestion.id_kfc);
                }
            });
        });
    </script>
    <!--end autocomplete-->
 </head>
 <body >
<div class="site-wrap" >
	<div class="site-mobile-menu">
		<div class="site-mobile-menu-header">
			<div class="site-mobile-menu-close mt-3">
				<span class="icon-close2 js-menu-toggle"></span>
			</div>
		</div>
		<div class="site-mobile-menu-body"></div>
	</div>
<!--header-->
	<header class="site-navbar py-3" role="banner">
		<div class="container" >
			<div class="row align-items-center">
				<div class="col-6 col-xl-4">
					<h1 class="mb-0"><a href="<?php echo base_url('index.php/dashboard02/index');?>" class="text-black h2 mb-0">Youth GBI Gama<span class="text-primary">.</span></a></h1>
				</div>
				<div class="col-10 col-md-6 d-none d-xl-block">
					<nav class="site-navigation position-relative text-right text-lg-center" role="navigation">
						<ul class="site-menu js-clone-nav mx-auto d-one d-lg-block">
							<li class="active">
								<a href="<?php echo base_url('index.php/dashboard02/index');?>">Home</a>
							</li>
							<li><a href="<?php echo base_url("/index.php/dashboard02/mbuh");?>">Kel Baru</a></li>
							<li class="has-children">
								<a href="#">Pelayan</a>
								<ul class="dropdown">
									<li><a href="<?php echo base_url('index.php/dashboard/alokasi');?>">Alokasi Pelayan</a></li>
									<li><a href="<?php echo base_url('index.php/dashboard/report_presensi_pelayan');?>">Report Pelayan</a></li>
									<li><a href="#">X3</a></li>
									<li class="has-children">
										<a href="#">4</a>
										<ul class="dropdown">
											<li><a href="#">a</a></li>
											<li><a href="#">b</a></li>
										</ul>
									</li>
								</ul>
							</li>
							
						</ul>
					</nav>
				</div>

				<div class="col-6 col-xl-2 text-right">
					<div class="d-none d-xl-inline-block">
						<ul class="site-menu js-clone-nav ml-auto list-unstyle d-flex text-right mb-0" data-class="social">
							<li>
								<a href="https://web.facebook.com/kingdomfunclub.youthparty" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
							</li>
							<li>
								<a href="https://www.instagram.com/kfclub.official/?hl=id" class="pl-0 pr-3"><span class="icon-instagram"></span></a>
							</li>
							<li>
								<a href="https://www.instagram.com/youthgbigama/?hl=id" class="pl-0 pr-3"><span class="icon-instagram"></span></a>
							</li>
						</ul>
					</div>

					<div class="d-inline-block d-xl-none ml-md-0 mr-auto py3" style="position: relative;top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>
				</div>
			</div>
		</div>
	</header>
<!--end header-->
<!--transparan-->
	<div class="site-blocks-cover overlay inner-page-cover" style="background-image: url('http://192.168.43.74/interior/fotograp/images/hero_bg_3.jpg');" data-stellar-background-ratio="0.5">
		<div class="container">
			<!--gambar: https://scontent.fcgk9-1.fna.fbcdn.net/v/t1.0-0/p180x540/80805198_10215258374074840_609391383253352448_o.jpg?_nc_cat=102&_nc_eui2=AeFP3-ap8-oEqOUpk1xWaPE9vN8j1__KA4R8vGqVqn8HRZKlzu3xced5S3IR7yI_O2su9Bh3eBkffdG0LQdmhHJ8na0j_QsJKmaB6CncrMnjfg&_nc_ohc=hA_CfFYXj6sAQlpoyEplcQCbqNSQQHCmQzuuzJqFIWnW-IIW9SEWSCQXA&_nc_ht=scontent.fcgk9-1.fna&oh=df8c977b7bb8c6f038309985edd813bc&oe=5E7181A6-->
<!--form presensi-->
	<!--php start-->
	<?php $atribut=array('id'=>'contact_form');echo form_open('dashboard02/input_direct_presensi',$atribut);?>
  	<?php $tgl_input=date("Y-m-d");?>
	<!--end php-->
	<div class="row align-items-center">
		<div class="col-md-12 align-items-center text-lg-center">
			<!--search auto complete kolom--->
			<input type="search" style="witdh:400px; font-size=150px;" class="autocomplete nama" name="nama_jemaat" id="autocomplete1" placeholder="Ketikan nama disini" autofocus required="required" size="35" /> <button type="reset" class="btn btn-danger" value="reset" name="reset"> <span class="glyphicon glyphicon-refresh">   Reset</span></button>
		</div>
		<div class="col-md-6 text-lg-right">
				<p>Nama</p>
				<input name="nama_jemaat2" placeholder="Nama" class="autocomplete"  type="text" id="v_nama_jemaat"  readonly/><br>
				<input name="tgl_presensi" class="form_control"  type="HIDDEN" value="<?php echo $tgl_input;?>" />
                <input name="id_kfc" placeholder="ID KFC" class="autocomplete"  type="text" id="v_id_kfc" hidden />
                <input name="alokasi" placeholder="alokasi" class="autocomplete"  type="text" id="v_alokasi" hidden />
                <p>Telepon</p>
                <input name="tlp" placeholder="Telepon" class="autocomplete"  type="text" id="v_tlp_jemaat" readonly /><br>
                <p>Komsel</p>
                <input name="komsel" placeholder="Komsel" class="autocomplete"  type="text" id="v_komsel" readonly/>
		</div>
		<div class="col-md-6 text-left">
				<p>Alamat</p>
				<textarea name="alamat" placeholder="Alamat" class="autocomplete"  id="v_alamat_jemaat" height="" readonly ></textarea><br>
	            <p>Tgl Lahir</p>
	            <input name="tgl_lahir" placeholder="Birthday" class="autocomplete"  type="text" id="v_tgl_lahir" readonly/>
	            <p>Instagram</p>
	            <input name="tgl_lahir" placeholder="Instagram" class="autocomplete"  type="text" id="v_fb_ig" readonly/>
		</div>
		<div class="col-md-12 align-items-center text-lg-center">
			<button type="submit" class="btn btn-success" value="submit" name="submit" width="100px">Hadir <span class="glyphicon glyphicon-send"></span></button>
			<p>mau edit profile ? bisa, isi presensi dulu lalu klik nama kalian di bawah ya...</p>
		</div>
	</div>
	

    <!---end form presensi--->
   </div>
   </div>
  
<!--end transparan-->
	<!--div class="site-block-profile-pic" data-aos="fade" data-aos-delay="200">
		<a href="About.html"><img src="2.jpg" alt="Image"></a>
	</div-->

	<div class="site-section" data-aos="fade">
		<div class="container">
			<div class="row mb-3">
				<div class="col-12">
					<h2 class="site-section-heading text-center">Siapa aja nih yang udah dateng..?</h2>
				</div>
			</div>
			<div class="row">
				<?php
                $no=1;
                if($data_get==NULL){?>
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <h4>Waduh, kok belum ada yang dateng ya...?</h4>
                
              </div>
              <?php }else{foreach($data_get as $dg){?>
                <?php //$warna=$dg->alokasi;
                 //if($warna=="vo"){
                 // echo "class='btn-warning'";}else{if($warna=="mu"){echo "class='btn-success'";}}?>
                 <div class="col-md-4">
					<div class="site-block-half d-lg-flex">
						<div class="image" style="background-image: url('<?php echo base_url('foto');?>/<?php echo $dg->foto; ?>');" data-aos="fade-up"></div>
						<div class="text">

                 <p><?php echo $no++;?>.
                  <a href="<?php echo base_url('index.php/dashboard02/profil');?>/<?php echo $dg->id_kfc; ?>"><?php echo $dg->nama_jemaat;?></a></p>
                  	<p><?php echo $dg->jenis_kelamin;?></p>
                  <p><?php  if($dg->komsel==""){
                    echo "Belum Komsel";
                  }else{
                    echo "Komsel ".$dg->komsel;
                  }?></p>
                 <p><?php  if($dg->alokasi==""){
                    echo "Belum Pelayanan";
                  }else{
                    echo $dg->alokasi;
                  }?></p>
                  <p><?php echo $dg->pemurid;?></p>
                  <p><?php echo $dg->jam_presensi;?></p>
                  </div>
					</div>
					</div>
              <?php } }?>
<!--enddata-->
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--script src="http://192.168.43.74/interior/1/js/jquery-3.3.1.min.js"></script>
<script src="http://192.168.43.74/interior/1/js/jquery-migrate-3.0.1.min.js"></script-->
<script src="<?php echo base_url();?>tema_baru/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>tema_baru/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url();?>tema_baru/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>tema_baru/js/aos.js"></script>
<script src="<?php echo base_url();?>tema_baru/js/main.js"></script>
<script>
	$(document).ready(function(){
		$('#lightgallery').lightgallery();
	});
</script>
 </body>
 </html>