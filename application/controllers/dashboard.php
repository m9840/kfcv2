<?php
ob_start();
class dashboard extends CI_Controller{

	function __construct(){

		parent::__construct();

		$this->load->model('m_youth_database');

		$this->load->library('autentifikasi');

		$this->autentifikasi->cek_autentifikasi();

		

	}

	function index(){

		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

		

		$status="1";

		$this->data_aktif['data_get']=$this->m_youth_database->ambil_data_youth($status);

		//page

		$this->load->view('head',$data);

	
		$this->load->view('home',$this->data_aktif);

		$this->load->view('footer');

	}

	function unactiv(){

		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

		

		$status="0";

		$this->data_aktif['data_get']=$this->m_youth_database->ambil_data_youth($status);

		//page

		$this->load->view('head',$data);

		$this->load->view('unactiv',$this->data_aktif);

		$this->load->view('footer');

	}

	function jemaat_baru(){

		

			$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

			$data=array('user'=>$ambil_admin);

			$this->kode_kfc['id_kfc']=$this->m_youth_database->id_kfc();

			//page

			$this->load->view('head',$data);

			$this->load->view('input_jemaat_baru',$this->kode_kfc);

			$this->load->view('footer');

	}

	function insert_jemaat(){ //v

		

		if($this->input->post('submit')){

			$this->load->model('m_youth_database');

			$this->m_youth_database->insert();

			redirect('dashboard','refresh');

		}

	}

	function edit($id_kfc=false) {

		if($this->input->post('submit')){

			$this->m_youth_database->edit($id_kfc);

			redirect('dashboard/direct_presensi','refresh');

		}

		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

		$data_youth['hasil']=$this->m_youth_database->getById($id_kfc);

		$this->load->view('head',$data);

		$this->load->view('edit',$data_youth);

		$this->load->view('footer');

	}

	function hapus($id_kfc=false){

		$this->load->model('m_youth_database');

		if($id_kfc){

			$this->m_youth_database->hapus($id_kfc);

		}

		redirect('dashboard');

	}

	function input_presensi(){

		if($this->input->post('submit')){

			$this->m_youth_database->simpan_presensi();

			redirect('dashboard');

		}else{

		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

		

		$status="1";

		$this->data_aktif['data_get']=$this->m_youth_database->ambil_data_youth($status);

		//page

		$this->load->view('head',$data);

		$this->load->view('input_presensi',$this->data_aktif);

		$this->load->view('footer');

		}

	}

	function cetak_presensi(){

			$status="1";

			$this->data_aktif['data_get']=$this->m_youth_database->ambil_data_youth($status);

			$this->load->view('cetak_presensi', $this->data_aktif);

	}

	function info(){

		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

		

		$bln_lahir=date("m");
		$bln_masuk=date("m");
		$thn_masuk=date("Y");

		$this->data_info['data_ultah']=$this->m_youth_database->ambil_data_ultah($bln_lahir);

		$this->data_info['data_baru']=$this->m_youth_database->ambil_data_keluarga_baru($bln_masuk,$thn_masuk);

		

		//page

		$this->load->view('head',$data);

		$this->load->view('info',$this->data_info);//,$this->data_ultah,$this->data_baru

		$this->load->view('footer');

	}
	function info_ultah(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

			if($this->input->post('submit')){


				$bln_lahir=$this->input->post('bln_lahir');

				$this->data_info['data_ultah']=$this->m_youth_database->ambil_data_ultah($bln_lahir);
				$this->load->view('head',$data);

				$this->load->view('ultah',$this->data_info,$bln_lahir);

				$this->load->view('footer');

			//redirect('dashboard','refresh');

		}else{
			$bln_lahir=date("m");

			$this->data_info['data_ultah']=$this->m_youth_database->ambil_data_ultah($bln_lahir);
				$this->load->view('head',$data);

				$this->load->view('ultah',$this->data_info);

				$this->load->view('footer');

		}
	}

	function info_keluarga_baru(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

			if($this->input->post('submit')){


				$tgl_awal=$this->input->post('tgl_awal');
				$tgl_akhir=$this->input->post('tgl_akhir');

				$this->data_info['data_ultah']=$this->m_youth_database->info_keluarga_baru($tgl_awal,$tgl_akhir);
				$this->load->view('head',$data);

				$this->load->view('keluarga_baru',$this->data_info);

				$this->load->view('footer');

			//redirect('dashboard','refresh');

		}else{
				$a=date("Y-m");
				
				$tgl_awal=$a."-01";
				$tgl_akhir=date("Y-m-d");

				$this->data_info['data_ultah']=$this->m_youth_database->info_keluarga_baru($tgl_awal,$tgl_akhir);
				$this->load->view('head',$data);

				$this->load->view('keluarga_baru',$this->data_info);

				$this->load->view('footer');

		}
	}

	function cetak_info($bln_lahir){

		 ob_start();

		 $bulan=date("m");

			$this->data_aktif['data_get']=$this->m_youth_database->ambil_data_ultah($bln_lahir);

			$this->load->view('cetak_info', $this->data_aktif);

			$html = ob_get_contents();

			ob_end_clean();

			 

			require_once('./assets/html2pdf.class.php');

			$pdf = new HTML2PDF('P','A4','en');

			$pdf->WriteHTML($html);

			$pdf->Output('Daftar Ultah '.$bulan.'.pdf', 'V');

	}
	
	

	function aktivasi($id_kfc=false){

		$this->load->model('m_youth_database');

		if($id_kfc){

			$this->m_youth_database->aktivasi($id_kfc);

		}

		redirect('dashboard');

	}

	function logout(){

		$this->session->sess_destroy();

		redirect('dashboard','refresh');

	}
	function direct_presensi(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

			$data=array('user'=>$ambil_admin);
			$now=date("Y-m-d");

		$this->data_aktif['data_get']=$this->m_youth_database->show_presensi($now);

		//$this->load->view('head',$data);

		$this->load->view('direct_presensi',$this->data_aktif);
		//$this->load->view('footer');
	}
	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_jemaat')->like('nama_jemaat',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->nama_jemaat,
				'nama_jemaat'	=>$row->nama_jemaat,
				'alamat_jemaat'	=>$row->alamat_jemaat,
				'tlp_jemaat'	=>$row->tlp_jemaat,
				'komsel'	=>$row->komsel,
				'alokasi'	=>$row->alokasi,
				'tgl_lahir'	=>$row->tgl_lahir,
				'id_kfc'	=>$row->id_kfc
			//// alias                  nama field pada tabel
			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	function input_direct_presensi(){
			$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

			$data=array('user'=>$ambil_admin);


		if($this->input->post('submit')){

			$this->m_youth_database->simpan_direct_presensi();

			redirect('dashboard/direct_presensi');

		}

		else{
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			redirect('dashboard/direct_presensi');
			//$now=date("Y-m-d");

		//$this->data_aktif['data_get']=$this->m_youth_database->show_presensi($now);

		//$this->load->view('head',$data);

		//$this->load->view('direct_presensi',$this->data_aktif);

			//$this->load->view("direct_presensi");
	}


	}
	FUNCTION report_presensi(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

			if($this->input->post('submit')){


				$tgl_presensi=$this->input->post('tgl_presensi');
				$now=$tgl_presensi;

				$this->data_info['data_presensi']=$this->m_youth_database->show_presensi($now);

				$this->load->view('head',$data);

				$this->load->view('report_presensi',$this->data_info);

				$this->load->view('footer');

			//redirect('dashboard','refresh');

		}else{
			$tgl_presensi=date("Y-m-d");
			$now=$tgl_presensi;
			$this->data_info['data_presensi']=$this->m_youth_database->show_presensi($now);
				$this->load->view('head',$data);

				$this->load->view('report_presensi',$this->data_info);

				$this->load->view('footer');

		}
	}
	FUNCTION report_presensi_pelayan(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

		$data=array('user'=>$ambil_admin);

			if($this->input->post('submit')){


				$tgl_presensi=$this->input->post('tgl_presensi');
				$now=$tgl_presensi;

				$this->data_info['data_presensi']=$this->m_youth_database->show_presensi($now);

				$this->load->view('head',$data);

				$this->load->view('report_presensi_pelayan',$this->data_info);

				$this->load->view('footer');

			//redirect('dashboard','refresh');

		}else{
			$tgl_presensi=date("Y-m-d");
			$now=$tgl_presensi;
			$this->data_info['data_presensi']=$this->m_youth_database->show_presensi($now);
				$this->load->view('head',$data);

				$this->load->view('report_presensi_pelayan',$this->data_info);

				$this->load->view('footer');

		}
	}
	function alokasi(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

			$data=array('user'=>$ambil_admin);
		

		$this->data_aktif['data_get']=$this->m_youth_database->show_alokasi();

		$this->load->view('head',$data);

		$this->load->view('alokasi',$this->data_aktif);
	}
	function simpan_alokasi(){
		$ambil_admin=$this->m_youth_database->ambil_admin($this->session->userdata('username'));

			$data=array('user'=>$ambil_admin);


		if($this->input->post('submit')){

			$this->m_youth_database->simpan_alokasi();

			redirect('dashboard/alokasi');

		}

		else{
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			redirect('dashboard/direct_presensi');}
	}
	function mbuh(){
		$this->kode_kfc['id_kfc']=$this->m_youth_database->id_kfc();
		$this->load->view("mbuh",$this->kode_kfc);
		}
	function input_mbuh(){
	if($this->input->post('submit')){

			$this->load->model('m_youth_database');

			$this->m_youth_database->insert_mbuh();
			$this->session->set_flashdata('msg', 
                '<div class="alert alert-success">
                    <h4>Terima Kasih</h4>
                    <p>Data Berhasil disimpan.</p>
					<p>Minggu depan langsung presensi ya..</p>
                </div>');
			redirect('dashboard/mbuh','refresh');

		}else{redirect('dashboard','refresh');}
		}
	

}

?>

		

				